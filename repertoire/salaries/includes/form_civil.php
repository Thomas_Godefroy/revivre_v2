<?php
    $query_fonctions = mysqli_query($db, "SELECT * FROM Fonction WHERE FCT_Id <> 0 ORDER BY FCT_Nom");
?>
<div class="repertoire-bloc">
	<fieldset>
		<legend>Coordonnées civiles<span class="required-fields-info">Champs obligatoires*</span></legend>
		<table class="form_table">
			<tr>
				<td><label for="PER_Nom">Nom* :</label></td>
				<td><input required="required" class="inputC" type="text" id="PER_Nom" name="PER_Nom" <?php echo isset($personne["PER_Nom"]) ? 'value="'.stripslashes($personne["PER_Nom"]).'"' : "";?>/></td>
				<td><label for="PER_Prenom">Prénom* :</label></td>
				<td><input required="required" class="inputC" type="text" id="PER_Prenom" name="PER_Prenom" <?php echo isset($personne["PER_Prenom"]) ? 'value="'.stripslashes($personne["PER_Prenom"]).'"' : "";?>/></td>
			</tr>
			<tr>
				<td><label for="PER_Sexe">Sexe* :</label></td>
				<td>
					<input required="required" type="radio" id="PER_Sexe" name="PER_Sexe" value="true" <?php echo (isset($personne["PER_Sexe"]) && $personne["PER_Sexe"] == "true") ? 'checked="checked"' : "";?>/>
					<label>Homme</label>
                    <input type="radio" id="PER_Sexe" name="PER_Sexe" value="false" <?php echo (isset($personne["PER_Sexe"]) && $personne["PER_Sexe"] == "false" ) ? 'checked="checked"' : "";?>/>
					<label>Femme</label>
				</td>
				<td><label for="PER_DateN">Date de Naissance* :</label></td>
				<td><input required="required" class="inputC" type="date" id="PER_DateN" name="PER_DateN" <?php echo isset($personne["PER_DateN"]) ? 'value="'.$personne["PER_DateN"].'"' : "";?>/></td>
			</tr>
			<tr>
				<td><label for="PER_LieuN">Lieu de Naissance :</label></td>
				<td><input class="inputC" type="text" id="PER_LieuN" name="PER_LieuN" <?php echo isset($personne["PER_LieuN"]) ? 'value="'.stripslashes($personne["PER_LieuN"]).'"' : "";?>/></td>
				<td><label for="PER_Nation">Nationalité :</label></td>	
				<td><input class="inputC" type="text" id="PER_Nation" name="PER_Nation" <?php echo isset($personne["PER_Nation"]) ? 'value="'.stripslashes($personne["PER_Nation"]).'"' : "";?>/></td>
			</tr>
			<tr>
				<td><label for="PER_Adresse">Rue, lotissement :</label></td>
				<td><input class="inputC" type="text" id="PER_Adresse" name="PER_Adresse" <?php echo isset($personne["PER_Adresse"]) ? 'value="'.stripslashes($personne["PER_Adresse"]).'"' : "";?>/></td>
				<td><label for="PER_Ville">Ville :</label></td>
				<td><input class="inputC" type="text" id="PER_Ville" name="PER_Ville" <?php echo isset($personne["PER_Ville"]) ? 'value="'.stripslashes($personne["PER_Ville"]).'"' : "";?>/></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><label for="PER_CodePostal">Code postal :</label></td>
				<!--  Modification V3.31 - mise en place d'un pattern (auparavant : min="1000" max="99999")  -->
				<td><input class="inputC" type="number" id="PER_CodePostal" name="PER_CodePostal"  pattern="[0-9]{5}" title="format XXYYY ou XXXYY - avec X et Y des chiffres de 0 à 9"
					<?php echo (isset($personne["PER_CodePostal"]) && $personne["PER_CodePostal"] > 999) ? 'value="'.$personne["PER_CodePostal"].'"' : "";?>/></td>
			</tr>
			<tr></tr>
			<tr>
				<td><label for="PER_NCaf">Numéro de CAF :</label></td>
				<td><input class="inputC" type="text" id="PER_NCaf" name="PER_NCaf" <?php echo isset($personne["PER_NCaf"]) ? 'value="'.$personne["PER_NCaf"].'"' : "";?>/></td>
				<td><label for="PER_NPoleEmp">Numéro Pôle Emploi :</label></td>
				<td><input class="inputC" type="text" id="PER_NPoleEmp" name="PER_NPoleEmp" <?php echo isset($personne["PER_NPoleEmp"]) ? 'value="'.$personne["PER_NPoleEmp"].'"' : "";?>/></td>
			</tr>
			<tr>
				<td><label for="PER_NSecu">Numéro de Sécurité Sociale* :</label></td>
				<td><input required="required" class="inputC" type="text" id="PER_NSecu" name="PER_NSecu" onblur="verifierNumeroDeSecuriteSocial(this)" <?php echo (isset($personne["PER_NSecu"]) && $personne["PER_NSecu"] != "")  ? 'value="'.socialSecurityNumberFormatting($personne["PER_NSecu"]).'"' : "";?>/></td>
			</tr>
			<tr>
				<td>
					<!-- L'activation de l'icone d'information s'effectue dans la fonction 'verifierNumeroDeSecuriteSocial' -->
					<img style="visibility:hidden;" alt="Information sur numéro de sécurité sociale" id="iconInfoIncoherenceNumSecuSoc" src="<?php echo $pwd; ?>/images/01_icons/dalerte-attention-avertissement-icone-8189-32.png">
				</td> 
				<td>
					<!-- L'activation du message d'information paramétrable par le biais du JS s'effectue
						 dans l'appel des sous fonctions de 'verifierNumeroDeSecuriteSocial' -->
					<span style="color:DarkOrange;" id="textInfoIncoherenceNumSecuSoc"></span>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><label for="PER_TelFixe">Téléphone Fixe :</label></td>
				<td><input class="inputC" type="text" id="PER_TelFixe" name="PER_TelFixe" maxlength="10" <?php echo isset($personne["PER_TelFixe"]) ? 'value="'.$personne["PER_TelFixe"].'"' : "";?>/></td>
				<td><label for="PER_TelPort">Téléphone Portable :</label></td>
				<td><input class="inputC" type="text" id="PER_TelPort" name="PER_TelPort" maxlength="10" <?php echo isset($personne["PER_TelPort"]) ? 'value="'.$personne["PER_TelPort"].'"' : "";?>/></td>
			</tr>
			<tr>
				<td><label for="PER_Fax">Fax :</label></td>
				<td><input class="inputC" type="text" id="PER_Fax" name="PER_Fax" maxlength="10" <?php echo isset($personne["PER_Fax"]) ? 'value="'.$personne["PER_Fax"].'"' : "";?>/></td>
				<td><label for="PER_Email">Adresse @ email :</label></td>
				<td><input class="inputC" type="text" id="PER_Email" name="PER_Email" <?php echo isset($personne["PER_Email"]) ? 'value="'.stripslashes($personne["PER_Email"]).'"' : "";?>/></td>
			</tr>

			<tr>
				<?php
					if($fonction && $_POST["TYP_Id"] <= 5){
				?>
				<td>
                    <label for="FCT_Id">Fonction* :</label>
                </td>
                <td style="position: relative;">
                    <div class="selectType">
                        <select id="FCT_Id" name="FCT_Id">
                            <option value="null" disabled="disabled" selected="selected">Choisir ...</option>
                        <?php
                        	while($data = mysqli_fetch_assoc($query_fonctions)){
                        ?>
                            <option value="<?php echo $data['FCT_Id']; ?>"<?php echo (isset($personne["FCT_Id"]) && $personne["FCT_Id"] == $data['FCT_Id']) ? ' selected="selected"' : "";?>><?php echo $data['FCT_Nom']; ?></option>
                        <?php
                        	}
                        ?>
                        </select>
                    </div>
                    <input type="button" value="+" id="addFunctionCross" class="delCross crossNextToField"/>
                </td>
                <?php
					}
					
					if($_POST["request_type"] == "edit"){
				?>
					<td><label for="SAL_DateSortie">Date de sortie :</label></td>
					<td><input type="date" id="SAL_DateSortie" name="SAL_DateSortie" class="inputC" step="1" <?php echo isset($personne["SAL_DateSortie"]) ? 'value="'.$personne["SAL_DateSortie"].'"' : "";?>></td>
				<?php
					}
				?>
			</tr>
		</table>
	</fieldset>
</div>
<?php
	if($fonction){
?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#addFunctionCross").on("click", function(){
			if($("#FCT_Id").prop("disabled")){
				$(this).val("+");
				$("table.form_table tr").last().remove();
				$("#FCT_Id").prop("disabled", false);
			}
			else{
				$(this).val("-");
				$("#FCT_Id").prop("disabled", true);
				$('<tr><td><strong>&#8618;</strong></td><td><input type="text" class="inputC" placeholder="Nouvelle fonction" required="required" id="new_FCT_Id" name="new_FCT_Id"/></td></tr>')
					.insertAfter($("#addFunctionCross").parent().parent());
			}
		});
		<?php
			if(isset($personne["new_FCT_Id"])){
		?>
			$("#addFunctionCross").trigger("click");
			$("#new_FCT_Id").val("<?php echo $personne["new_FCT_Id"]; ?>");
			if(inputErrorList.new_FCT_Id != null){
				$("#new_FCT_Id").addClass("value-error");
			}
		<?php
			}
		?>
	});
</script>
<?php
	}
?>

<!-- MAJ V3.32 de l'app - modifications des formulaires (vérification sur numéro de sécu) 
		Il doit s'agir UNIQUEMENT d'un message informatif (ne bloque pas l'envoi du formulaire)-->
<script type="text/javascript">

	var infoMessageNumSecToDisplay = "";
	var isAlreadyDisplayNumSecuErrorMessage = false;

	function verifierNumeroDeSecuriteSocial(formCivil)
	{
		/* On ne test le numéro de sécurité sociale que si la personne est française.
		En effet, l'association accueil des personnes de nationalité étrangère (= 'numéro' avec des lettres possibles) */
	
		var isNationaliteFrancaise = false;

		/*	Modification du champ qui est un objet en String. charAt(0) = ' " '...
			Suppression avec le replace de tous les "blancs de formatage" (regroupements des chiffres: 1 / 2 / 2 / 2 / 3 / 3 / 2) */
		var numeroSecu = ((JSON.stringify(formCivil.PER_NSecu.value)).replace(/\s/g,"")).toUpperCase();

		isNationaliteFrancaise = verifierNationaliteFrancaise(formCivil.PER_Nation);

		if(!isNationaliteFrancaise) {
			return true;
		}else if(!verifierValiditeCleNIR(formCivil.PER_NSecu, numeroSecu)) {
				//Le numéro n'est pas valide, on affiche le message d'avertissement
				document.getElementById("textInfoIncoherenceNumSecuSoc").innerHTML = infoMessageNumSecToDisplay;
				document.getElementById("iconInfoIncoherenceNumSecuSoc").style.visibility = "visible";
				
			if(isAlreadyDisplayNumSecuErrorMessage) {
				return true;
			}else {
				alert("Le numéro de sécurité sociale est erroné. Veuillez vous référer aux coordonnées civiles.");
				
				/*	La consigne du client : ne pas avoir quelque chose de contraignant, 
				donc on affiche 1 erreur, il ne doivent pas avoir a cliquer 10 fois pour un num de secu incoherent */
				isAlreadyDisplayNumSecuErrorMessage = true;
				
				return false;
			}
		}else {
			return true;
		}
	}
	function verifierNationaliteFrancaise(champPER_Nation) {

		var nationalite = champPER_Nation.value;
		
		//modification du formatage pour que la comparaison de string fonctionne
		nationalite = (nationalite.replace("ç", "c")).toUpperCase();

		//On recherche la chaine "FRANC" pour prendre en compte le "franco-xxx"
		if (nationalite.indexOf("FRANC") != -1 ) {
			  return true;
		}else {
			return false;
		}
	}
	function verifierPresenceDeCaracteres(champPER_NSecu, numeroSecu) {

		var PresenceCaractere = false;
		var i = 1;		//On ignore le caractere à l'indice 0, car il s'agit du ' " '
		
		while ( (numeroSecu.charAt(i) != "\"") && (PresenceCaractere == false) ) {
			//Tentative de conversion de 1 caractere en entier en base decimale, si echec retour de la valeur 'NaN'
			if(isNaN(parseInt(numeroSecu.charAt(i), 10))){
				PresenceCaractere = true;
			}
			++i;
		}
		surlignerChamp(champPER_NSecu, PresenceCaractere);

		if(PresenceCaractere){
			infoMessageNumSecToDisplay = "Le numéro de sécurité sociale semble contenir des lettres.";
			return false;
		}else {
			return true;
		}
	}
	function verifierValiditeCleNIR(champPER_NSecu, numeroSecu) {
		
		//La clé NIR est calculée sur des valeurs numériques. Il faut remplacer les lettres présentes dans les départements corses		
		numeroSecu = convertirDepartementCorse(numeroSecu);
		
		if(!verifierPresenceDeCaracteres(champPER_NSecu, numeroSecu)){
			infoMessageNumSecToDisplay = "Erreur sur calcul clé NIR.";
			return false;
		}else {
			if( calculCleSecuriteSociale(numeroSecu) == parseInt(numeroSecu.substring(14,16))){
				return true;
			}else {
				infoMessageNumSecToDisplay = "Erreur sur calcul clé NIR.";
				return false;
			}
		}		
	}
	function calculCleSecuriteSociale(numeroSecu){
		//Calcul la clé NIR du numéro de sécu, à partir des 13 premiers chiffres

		return (97 - (parseInt(numeroSecu.substring(1, 14)) % 97));
	}
	function convertirDepartementCorse(numeroSecu){
		if("2A" == numeroSecu.substring(6, 8)) {			
			numeroSecu = numeroSecu.replace("2A", "19");
			
		}else if("2B" == numeroSecu.substring(6, 8)) {			
			numeroSecu = numeroSecu.replace("2B", "18");
		}

		return numeroSecu;
	}
	function surlignerChamp(champ, erreur)
	// Si erreur vaut true, cette fonction colore le champ en orange. Sinon, elle rétablie la couleur de l'arrière plan
	{
	   if(erreur == true){
	      champ.style.backgroundColor = "#FFA500";
	   }
	   else {
		   //Couleur par défaut d'un champ du formulaire
	      champ.style.backgroundColor = "#cde5f7";
	   }
	}
	

</script>
