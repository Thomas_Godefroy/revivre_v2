<?php

function set_number_lines_per_day($max_number_people, &$nb_lines_per_day) {
	//En fonction du nombre de personnes sur un creneau, determine le nombre de lignes requises pour afficher une journee

	$ind_cre = 0;

	$total_nb_lines_week_morning = 0;
	$total_nb_lines_week_afternoon = 0;

	for($jour = 0; $jour < 5; $jour ++) {

		if (isset ( $max_number_people [$ind_cre] ["numberSal"] ) == true) {
			$nb_lines_morning = $max_number_people [$ind_cre ++] ["numberSal"];
			$total_nb_lines_week_morning += $nb_lines_morning;
		} else {
			$ind_cre ++;
			$nb_lines_morning = 0;
		}
		if (isset ( $max_number_people [$ind_cre] ["numberSal"] ) == true) {
			$nb_lines_afternoon = $max_number_people [$ind_cre ++] ["numberSal"];
			$total_nb_lines_week_afternoon += $nb_lines_afternoon;
		} else {
			$ind_cre ++;
			$nb_lines_afternoon = 0;
		}
		array_push ( $nb_lines_per_day, (($nb_lines_morning > $nb_lines_afternoon) ? $nb_lines_morning : $nb_lines_afternoon) );
	}
}

function adjust_number_lines_week(&$nb_lines_per_day, &$nb_lines_week){
	//Pour ne pas avoir de probleme � l'impression et marquer une separation pour les jour, il faut un nombre de lignes minimum
	
	for($jour = 0; $jour < 5; $jour ++) {
		while ( $nb_lines_per_day[$jour] < MIN_NB_OF_LINES_PER_DAY ) {
			$nb_lines_per_day [$jour] ++;
		}
		$nb_lines_week += $nb_lines_per_day[$jour];
	}
}

function check_if_number_of_lines_exceed_format($nb_lines_week, $max_nb_lines_for_paper_size) {
	//Si le nombre de lignes depasse le format A4 ou A3, ajout de lignes pour passage sur une autre page
	
	while($nb_lines_week > $max_nb_lines_for_paper_size) {
		$max_nb_lines_for_paper_size += OVERAGE_LINES;
	}
	return $max_nb_lines_for_paper_size;
}

function fill_empty_space_with_unused_lines($max_nb_lines_for_paper_size, &$nb_lines_week, &$nb_lines_per_day){
	// Complete avec des lignes vides le planning jusqu'a obtenir le nombre de lignes pour le format A3 ou A4 specifie
	
	while ( ($max_nb_lines_for_paper_size - $nb_lines_week) > 0 ) {
		for($jour = 0; $jour < 5; $jour ++) {
			if (($jour == 0) || ($nb_lines_per_day [$jour] < $nb_lines_per_day [$ind_min_lines_day])) {
				$ind_min_lines_day = $jour;
			}
		}
		$nb_lines_per_day [$ind_min_lines_day] ++;
		$nb_lines_week ++;
	}
}


// Lien relatif vers la racine du repertoire projet
$pwd = "../";

// On verifie que la session possede les caracteristiques du planning a afficher, sinon on redirige sur l'ecran de selection d'un planning
if (isset ( $_POST ['ENC_Num'] ) && isset ( $_POST ['ASSOC_Date'] ) && isset ( $_POST ["PL_id"] ) && isset ( $_POST ["Page_Format"] )) {
	
	include ($pwd . 'assets.php');
	
	error_reporting ( E_ALL );
	ini_set ( 'display_errors', TRUE );
	ini_set ( 'display_startup_errors', TRUE );
	$db = revivre (); // Connection a la base de donnee (fonction dans assets.php)
	mysqli_query ( $db, "SET NAMES 'utf8'" );
	
	/*
	 * L'association ne peut avoir plus de 15 personnes par jour par encadrant (+ ou - contrainte legale)
	 * Cependant, il arrive parfois de depasser legerement cette valeur, donc impression sur plusieurs pages a faire
	 */
	$max_line_per_days = ($_POST ["Page_Format"] == "A3") ? 15 : 10;
	/*
	 * On affiche systematiquement les 5 jours ouvres de la semaine, avec une police lisible de loin.
	 * Donc 10*5 et 15*5 lignes sur une page, respectivement A4 et A3
	 */
	$max_nb_lines_for_paper_size = ($_POST ["Page_Format"] == "A3") ? 75 : 50;
	// Constante a appliquer si l'on depasse le nombre de lignes max sur le format selectionne
	define ( 'OVERAGE_LINES', 25 );
	define ( 'MIN_NB_OF_LINES_PER_DAY', 4 );
	
	// Selection de tous les libelles de planning (GOB, SOB, AVA...)
	$query = mysqli_query ( $db, "SELECT PL_Libelle FROM typeplanning ORDER BY PL_id" );
	$nomTypesPlanning = mysqli_fetch_all ( $query, MYSQLI_ASSOC );
	
	$listeJours = array (
			"Lundi",
			"Mardi",
			"Mercredi",
			"Jeudi",
			"Vendredi" 
	);
	
	// On recupere toutes les personnes en lien avec un planning specifique
	$query = mysqli_query ( $db, "SELECT concat(PER_Nom, ' ',PER_Prenom) AS nom, CNV_Couleur, CRE_id FROM pl_association
                                JOIN salaries USING(SAL_NumSalarie)
                                JOIN personnes USING(PER_Num)
                                JOIN insertion USING(SAL_NumSalarie)
                                JOIN convention USING(CNV_id)
                                WHERE ASSOC_date = '" . $_POST ["ASSOC_Date"] . "' AND ENC_Num = " . $_POST ["ENC_Num"] . " AND PL_id = " . $_POST ["PL_id"] . " ORDER BY CRE_id, nom" );
	$planningContenu = mysqli_fetch_all ( $query, MYSQLI_ASSOC );
	
	// On recupere les proprietes en lien avec un planning specifique
	$query = mysqli_query ( $db, "SELECT DISTINCT ASSOC_Couleur, ASSOC_AM, ASSOC_PM, ASSOC_LastEdit FROM pl_proprietees 
                                WHERE ENC_Num = " . $_POST ["ENC_Num"] . " AND ASSOC_Date = '" . $_POST ["ASSOC_Date"] . "' AND PL_id = " . $_POST ["PL_id"] . ";" );
	$proprietees = mysqli_fetch_assoc ( $query );
	
	// On recupere les logos en lien avec un planning specifique
	$query = mysqli_query ( $db, "SELECT lo.LOGO_Url FROM pl_logo plo JOIN logo lo ON lo.LOGO_id = plo.LOGO_Id
                                WHERE plo.ENC_Num = " . $_POST ["ENC_Num"] . " AND plo.ASSOC_Date = '" . $_POST ["ASSOC_Date"] . "' AND PL_id = " . $_POST ["PL_id"] . ";" );
	$logos = mysqli_fetch_all ( $query, MYSQLI_ASSOC );
	
	// On recupere le nom de l'encadrant en lien avec un planning specifique
	$query = mysqli_query ( $db, "SELECT concat(PER_Nom,' ',PER_Prenom) AS Nom FROM salaries JOIN personnes USING(PER_Num) WHERE SAL_NumSalarie = " . $_POST ["ENC_Num"] );
	$nomEncadrant = mysqli_fetch_assoc ( $query );
	
	$too_much_people = false;
	
	// On compte le nombre maximum de personnes par journees, dans la semaine concernee par le planning
	$query = "SELECT count(*) AS numberSal FROM pl_association WHERE ASSOC_date = '" . $_POST ["ASSOC_Date"] . "' AND ENC_Num = '" . $_POST ["ENC_Num"] . "' AND PL_id = '" . $_POST ["PL_id"] . "' GROUP BY CRE_ID;";
	$result = mysqli_query ( $db, $query );
	$max_number_people = mysqli_fetch_all ( $result, MYSQLI_ASSOC );
	
	$nb_lines_per_day = array ();
	$nb_lines_week = 0;
	
	set_number_lines_per_day($max_number_people, $nb_lines_per_day);
	
	adjust_number_lines_week($nb_lines_per_day, $nb_lines_week);
	
	$max_nb_lines_for_paper_size = check_if_number_of_lines_exceed_format($nb_lines_week, $max_nb_lines_for_paper_size);
	
	fill_empty_space_with_unused_lines($max_nb_lines_for_paper_size, $nb_lines_week, $nb_lines_per_day);
	
	// demarre la temporisation de sortie. Tant qu'elle est enclenchee, aucune donnee, hormis les en-tetes,
	// n'est envoyee au navigateur, mais temporairement mise en tampon. (fonction de PHP)
	ob_start ();
	?>
<link rel="stylesheet" type="text/css" href="../css/planning.css" />
<page backtop="20mm" backbottom="5mm" backleft="2mm" backright="8mm"> <page_header
	pageset="old">
<div class="planning-printer-header">
	<h2>PLANNING <?php echo strtoupper($nomTypesPlanning[$_POST["PL_id"]-1]["PL_Libelle"]); ?> DU <?php echo date('d/m/Y',strtotime($_POST['ASSOC_Date'])) ?> AU <?php echo date('d/m/Y',strtotime($_POST['ASSOC_Date']." + 4 day")) ?></h2>
	<h4>Encadrant : <?php echo $nomEncadrant["Nom"] ?></h4>
</div>
</page_header> <page_footer pageset="old">
<div class="planning-printer-footer">
	<div class="planning-printer-logo">
                <?php
	for($logo_index = 0; $logo_index < sizeof ( $logos ); $logo_index ++) {
		echo '<img src="' . $pwd . $logos [$logo_index] ["LOGO_Url"] . '"/>';
	}
	?>
            </div>
	<h4>Affiché le <?php echo date("d/m/Y") ?>, Association Revivre Service CAP, Chemin de Mondeville - 14460 COLOMBELLES</h4>
</div>
</page_footer>

<table id="planning-table">
	<thead>
		<tr style="background-color: <?php echo $proprietees["ASSOC_Couleur"] ?>;">
			<th></th>
			<th>Matin<br /><?php echo $proprietees["ASSOC_AM"] ?></th>
			<th>P</th>
			<th>Après-midi<br /><?php echo $proprietees["ASSOC_PM"] ?></th>
			<th>P</th>
		</tr>
	</thead>
	<tbody>
<?php
	// Fixe l'id du creneau a lundi matin et permet de parcourir les personnes a placer sur le planning
	$CRE_id = 1;
	$num_personne = 0;
	
	// Boucle pour l'affichage des 5 jours ouvres de la semaine
	for($jour = 0; $jour < 5; $jour ++) {
		// Debut de ligne representant 1 jour
		echo '<tr>';
		$dateJourCourant = strtotime ( $_POST ["ASSOC_Date"] . ' + ' . $jour . ' day' );
		
		// -- Debut Colonne numero 1 --
		// -> Contient "Nom de la journee + date" ou "Nom de la journee + mention ferie"
		if (isJourFerie ( date ( "d/m/Y", $dateJourCourant ) )) {
			echo '<td class="firstColumn"><b>' . $listeJours [$jour] . '<br>FÉRIÉ</b></td>';
		} else {
			echo '<td class="firstColumn"><b>' . $listeJours [$jour] . '<br>' . date ( "d/m", $dateJourCourant ) . '</b></td>';
		}
		// -- Fin Colonne numero 1 --
		
		// Boucle pour l'affichage des creneaux "MATIN" et "APRES-MIDI" sur la meme ligne
		for($creneau_index = 0; $creneau_index < 2; $creneau_index ++) {
			$lineCount = 0;
			
			// -- Debut Colonne numero 2 --
			// -> Contient un TABLEAU de la liste des personnes present sur un creneau a un jour donne
			echo '<td><table>';
			// CRE_id du type : "lundi matin", "jeudi apres midi..."
			while ( isset ( $planningContenu [$num_personne] ) && $planningContenu [$num_personne] ["CRE_id"] == $CRE_id ) {
				if ($lineCount + 1 == $nb_lines_per_day [($jour)]) {
					// Dernier nom possible dans le tableau, affiche du nom SANS separateur horizontal en pointille
					echo '<tr><td style="color: ' . $planningContenu [$num_personne] ["CNV_Couleur"] . ';">' . stripslashes ( $planningContenu [$num_personne ++] ["nom"] ) . '</td></tr>';
				}else {
					// affichage du nom avec un separateur horitonzal en pointille
					echo '<tr><td style="color: ' . $planningContenu [$num_personne] ["CNV_Couleur"] . ';" class="separator">' . $planningContenu [$num_personne ++] ["nom"] . '</td></tr>';
				}
				$lineCount ++;
			}
			// Afichage des lignes vides lorsqu'il n'y a plus de personnes enregistrees sur un creneau
			for($number_of_lines = 0; $number_of_lines < ($nb_lines_per_day [($jour)] - $lineCount); $number_of_lines ++) {
				if ($number_of_lines + 1 < ($nb_lines_per_day [($jour)] - $lineCount)) {
					echo '<tr><td class="separator"></td></tr>';
				} else {
					echo '<tr><td></td></tr>';
				}
			}
			// Fin du tableau pour un creneau sur un jour donne
			echo '</table></td><td class="thinColumn"><table>';
			
			// Affichage de la colonne d'emargement entre les deux creneaux
			for($number_of_lines = 0; $number_of_lines < ($lineCount + ($nb_lines_per_day [($jour)] - $lineCount)); $number_of_lines ++) {
				if ($number_of_lines + 1 < ($lineCount + ($nb_lines_per_day [($jour)] - $lineCount))) {
					echo '<tr><td class="separator"></td></tr>';
				} else {
					echo '<tr><td></td></tr>';
				}
			} // Fin du tableau representant la colonne d'emargement
			echo '</table></td>';
			// Creneau traite, passage au suivant
			$CRE_id ++;
		}
		// Fin de ligne correspondant a un jour ouvre
		echo '</tr>';
	}
	?>
        </tbody>
</table>
</page>
<?php
	
	// parametrage de l'imprimante html2pdf
	$title = "planning_" . $nomTypesPlanning [$_POST ["PL_id"] - 1] ["PL_Libelle"] . "_" . str_replace ( " ", "_", $nomEncadrant ["Nom"] ) . "_" . date ( 'd-m-Y', strtotime ( $_POST ['ASSOC_Date'] ) ) . ".pdf";
	$content = ob_get_clean ();
	
	require_once ('../stuff/html2pdf/html2pdf.class.php');
	$html2pdf = new HTML2PDF ( 'P', $_POST ["Page_Format"], 'fr' );
	$html2pdf->pdf->SetAuthor ( 'Association Revivre' );
	$html2pdf->pdf->SetTitle ( 'Planning ' . strtoupper ( $nomTypesPlanning [$_POST ["PL_id"] - 1] ["PL_Libelle"] ) . ' - ' . date ( 'd/m/Y', strtotime ( $_POST ['ASSOC_Date'] ) ) );
	$html2pdf->pdf->SetSubject ( 'Planning hebdomadaire, Association Revivre' );
	$html2pdf->WriteHTML ( $content );
	$html2pdf->Output ( $title );
	exit ();
} else {
	echo '<script type="text/javascript">
        setTimeout(function(){
            $.redirect("./planningShowcase.php", {"ENC_Num": ' . $_POST ['ENC_Num'] . ', "ASSOC_Date": "' . $_POST ['ASSOC_Date'] . '", "PL_id": ' . $_POST ['PL_id'] . '})
        }, 2500);
    </script>';
}

?>